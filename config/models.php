<?php
 return[
     
    "models"		=> [
        "default_users"		=> [
            "class"		=> "\/App\/Models\/Defaults\/User"
        ],
        "default_Files"		=> [
            "class"		=> "\/App\/Models\/Defaults\/FileStorage"
        ],
        "default_Activities"		=> [
            "class"		=> "\/App\/Models\/Defaults\/Activities"
        ],
        "default_Articles"		=> [
            "class"		=> "\/App\/Models\/Defaults\/Articles"
        ],


        "ApiSchemas"		=> [
            "class"		=> "\/App\/Models\/Api\/ApiSchemas"
        ],
        "ApiModels"		=> [
            "class"		=> "\/App\/Models\/Api\/ApiModels"
        ],
        "ApiModelLevel0"		=> [
            "class"		=> "\/App\/Models\/Api\/ApiModelLevel0"
        ],
        "ApiModelLevel1"		=> [
            "class"		=> "\/App\/Models\/Api\/ApiModelLevel1"
        ],
        "ApiModelLevel2"		=> [
            "class"		=> "\/App\/Models\/Api\/ApiModelLevel2"
        ],
        "ApiModelLevel3"		=> [
            "class"		=> "\/App\/Models\/Api\/ApiModelLevel3"
        ],
        "ApiModelLevel4"		=> [
            "class"		=> "\/App\/Models\/Api\/ApiModelLevel4"
        ],
    ]
];