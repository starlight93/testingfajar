<?php
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use WebSocket\Client as ws;
use Telegram\Bot\Api as Tg;
use Telegram;
// 850255108:AAEhTmbS2uEE7J5dptoPNaUIloPzhJl1OXg
// https://api.telegram.org/bot850255108:AAEhTmbS2uEE7J5dptoPNaUIloPzhJl1OXg/setWebhook?url=https://backend.dejozz.com/chatbot/public/telegram-webhook
// { 
//     "update_id":955696541,
//     "message":{ 
//        "message_id":17,
//        "from":{ 
//           "id":402325749,
//           "is_bot":false,
//           "first_name":"Fajar",
//           "last_name":"Firmansyah",
//           "username":"firmansyah2018",
//           "language_code":"en"
//        },
//        "chat":{ 
//           "id":402325749,
//           "first_name":"Fajar",
//           "last_name":"Firmansyah",
//           "username":"firmansyah2018",
//           "type":"private"
//        },
//        "date":1578002125,
//        "text":"ewf"
//     }
//  }
$router->get('/', function () use ($router) {
    return response()->json(["info"=>"welcome to our chatbot"]);
});
$router->post('/telegram-webhook', function (Request $req) use ($router) {
    try{
        $data = $req->all();
        $chatId = $data['message']['chat']['id'];
        $userName = $data['message']['chat']['username'];
        $msgText = $data['message']['text'];
        
        $tg = new Tg(config("telegram.webhook_token")) ;
        $params = [  'chat_id'   => $chatId,"text"=>"$userName, anda mengirim: \n".($msgText) ];
        $tg->setAsyncRequest(true)->sendMessage($params);

    }catch(Exception $e){
        $tg = new Tg(config("telegram.bot_token"));
        $params = [  'chat_id'   => '402325749',"text"=>$e->getMessage()];
        $tg->setAsyncRequest(true)->sendMessage($params);
    }
    
});
//==================
// webhook https://backend.dejozz.com/chatbot/public/line-webhook
// {
// "events": [
//   {
//     "type": "message",
//     "replyToken": "a776a25a2e6e468aab6b0ee5e60b2ad8",
//     "source": {
//         "userId": "U1732035f58338576407894768175ef09",
//         "type": "user"
//     },
//     "timestamp": 1578007456220,
//     "mode": "active",
//     "message": {
//         "type": "text",
//         "id": "11191676682206",
//         "text": "coba"
//     }
//  }
// ],
// "destination": "U63a0860f69fc0b41d8ebd27878373c6c"
// }
$router->post('/line-webhook', function (Request $req) use ($router) {
    try{
        $data = $req->events[0];
        $replyToken = $data['replyToken'];
        $message    = $data['message']['text'];
        // $tg = new Tg(config("telegram.bot_token"));
        // $params = [  'chat_id'   => '402325749',"text"=>json_encode($data)];
        // $tg->setAsyncRequest(true)->sendMessage($params);
        $httpClient = new \LINE\LINEBot\HTTPClient\CurlHTTPClient(config("line.webhook_token"));
        $bot = new \LINE\LINEBot($httpClient, ['channelSecret' => config('line.channel_secret')]);
        $textMessageBuilder = new \LINE\LINEBot\MessageBuilder\TextMessageBuilder("pesan anda:\n$message");
        $response = $bot->replyMessage($replyToken, $textMessageBuilder);
        if (!$response->isSucceeded()) {            
            $tg = new Tg(config("telegram.bot_token"));
            $params = [  'chat_id'   => '402325749',"text"=>'balasan line gagal'];
            $tg->setAsyncRequest(true)->sendMessage($params);
        }

    }catch(Exception $e){
        $tg = new Tg(config("telegram.bot_token"));
        $params = [  'chat_id'   => '402325749',"text"=>$e->getMessage()];
        $tg->setAsyncRequest(true)->sendMessage($params);
    }
    
});
//==================
$router->get('/qazwsxedc', function(Request $request){
    $tg = new Tg(config("telegram.bot_token"));
    $params = [  'chat_id'   => '402325749',"text"=>'a'];
    $tg->setAsyncRequest(true)->sendMessage($params);
    // $telegram = new Tg('849705214:AAGSHiXgw5Xex2cdK-0xcYLbZE8z2KUIzNE');
    // $updates = $telegram->getWebhookUpdates();
    // $result = $telegram->getWebhookUpdates();
    // $params = [ "chat_id"=>$result['message'] ['chat']['id'],"text"=>'masuk'];
    // $telegram->setAsyncRequest(true)->sendMessage($params);
    // $result = $telegram->getData();
    // $text = $result['message'] ['text'];
    // $chat_id = $result['message'] ['chat']['id'];
    // $username=$result['message'] ['chat']['username'];
    // $params = array_merge(config("telegram.$to"), ["text"=>$message]);
});
$router->post('/websockets', function(Request $request){
    // return true;
    // if($request->id=="999"){
    //     return true;
    // }
    // $request->merge(['id'=>"999"]);
    if($request->mode){
        if($request->mode=='passive'){
            return true;
        }
    }
    $data = json_encode($request->all());
    $tg = new Tg("716800967:AAFOl7tmtnoBHIHD4VV_WfdFfNhfRZz0HGc");
    $params = [  'chat_id'   => '-345232929',"text"=>$data];
    $tg->setAsyncRequest(true)->sendMessage($params);
    return true;
});

$router->get('/websockets', function(Request $request){
    try{
    	$data = http_build_query($request->all());
    	$curl = curl_init();
    	curl_setopt($curl, CURLOPT_URL, "http://36.37.87.66/_redirect_pysock.php?$data");
    	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, True);
    	curl_setopt($curl, CURLOPT_RETURNTRANSFER, True);
        $data = curl_exec($curl);
    	
    }catch(Exception $e){
    	echo "gagal";
    }
    curl_close($curl);
    echo "success";
    die();
});

$router->get('/get-draft-id', function(){
    return crc32(uniqid());
});

$router->get("/websocket-send/{channel}", function(Request $req, $channel=999){
    try{
        $data = json_encode($req->all());
        $output = passthru("python3 /opt/others/wss/client.py $channel '$data'");
    }catch(Exception $e){
        return response()->json(["error"=>$e->getMessage()],422);
    }
    return $output;
});
$router->post("/websocket-send/{channel}", function(Request $req, $channel=999){
    try{
        $data = json_encode($req->all());
        $output = passthru("python3 /opt/others/wss/client.py $channel '$data'");
    }catch(Exception $e){
        return response()->json(["error"=>$e->getMessage()],422);
    }
    return $output;
});