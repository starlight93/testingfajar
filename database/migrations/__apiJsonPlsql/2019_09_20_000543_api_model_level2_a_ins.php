<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Helpers\PLSQL as PLSQL;
use App\Helpers\DBS as DBS;

class ApiModelLevel2AIns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {  
        $command=DBS::command(
            [    
                DBS::table('api_models')
                    ->selectRaw("COUNT(1) INTO _exist")
                    ->where("level",2)
                    ->where("process","NEW.process")
                    ->where("model","NEW.table")
                ,
                "IF _exist=0 THEN ",           
                    DBS::invoke_table('api_models')
                    ->invoke_insert([
                        'model'      => "NEW.table",
                        'process'    => "NEW.process",
                        'level'      => 2,
                        'type'       => "NEW.type",
                        'created_at' => "NEW.created_at",
                    ]),
                "END IF;"
            ])->create(); 
        PLSQL::table('api_model_level2')
                ->after('insert')
                ->declare([
                    '_exist'    => 'integer'
                ])
                ->script($command)
                ->create();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        PLSQL::table("api_model_level2")
                ->after('insert')
                ->drop();
    }
}
