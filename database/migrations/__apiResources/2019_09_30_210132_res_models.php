<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ResModels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('res_models', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('project')->nullable();
            $table->string('model')->nullable();
            $table->tinyInteger('level')->nullable();
            $table->smallInteger('user_id')->nullable();
            $table->tinyInteger('is_public')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('res_models');
    }
}
