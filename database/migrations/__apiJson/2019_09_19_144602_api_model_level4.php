<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ApiModelLevel4 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {        
        Schema::create('api_model_level4', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('parent_id');
            $table->string('process')->nullable();
            $table->string('table')->nullable();
            $table->string('type',15)->nullable();
            $table->json('data')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('api_model_level4');
    }
}
