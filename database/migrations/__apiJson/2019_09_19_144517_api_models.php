<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ApiModels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {        
        Schema::create('api_models', function (Blueprint $table) {
            $table->increments('id');
            $table->string('process')->nullable();
            $table->string('type',15)->nullable();
            $table->string('model')->nullable();
            $table->smallInteger('level')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('api_models');
    }
}
