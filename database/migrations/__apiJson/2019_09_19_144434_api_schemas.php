<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ApiSchemas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {        
        Schema::create('api_schemas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('project')->nullable();
            $table->string('process')->nullable();
            $table->string('model')->nullable();
            $table->string('type',15)->nullable();
            $table->json('schema')->nullable();
            $table->smallInteger('userid')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('api_schemas');
    }
}
