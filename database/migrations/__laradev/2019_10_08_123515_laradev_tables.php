<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LaradevTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laradev_tables', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('laradev_forms_id')->nullable();
            $table->mediumInteger('laradev_dbconnections_id')->nullable();
            $table->string('connection_name')->nullable();
            $table->string('project')->nullable();
            $table->string('form_id')->nullable();
            $table->string('name')->nullable();
            $table->string('type')->nullable();
            $table->json('actual_indexes')->nullable();
            $table->json('actual_FK')->nullable();
            $table->json('actual_columns')->nullable();
            $table->json('migrated_columns')->nullable();
            $table->json('old_columns')->nullable();
            $table->json('current_columns')->nullable();
            $table->json('attributes')->nullable();
            $table->string('status')->nullable();
            $table->boolean('is_updated')->nullable();
            $table->integer('user_id')->nullable();
            $table->datetime('last_edit')->nullable();
            $table->datetime('last_migrate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laradev_tables');
    }
}
