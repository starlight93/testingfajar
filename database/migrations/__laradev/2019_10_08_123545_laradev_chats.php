<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LaradevChats extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laradev_chats', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('room')->nullable();
            $table->string('type')->nullable();
            $table->text('text')->nullable();
            $table->integer('recipient_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laradev_chats');
    }
}
