<?php

use Illuminate\Database\Seeder;
use App\Models\Defaults\User;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $hasher = app()->make('hash');
        // DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        // $this->call('UsersTableSeeder');
        User::truncate();
        // factory(User::class, 5)->create();
        User::create(
            [
                'name' => "trial",
                'email' => "trial@trial.trial",
                'username'=>"trial",
                'password' => $hasher->make("trial")
            ]
        );
        // DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
