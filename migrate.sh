php artisan migrate:fresh --path=database/migrations/__defaults;
php artisan migrate --path=database/migrations/__apiJson;
php artisan migrate --path=database/migrations/__apiJsonPlsql;
php artisan migrate --path=database/migrations/__apiResources;
php artisan migrate --path=database/migrations/__apiResourcesPlsql;
php artisan migrate --path=database/migrations/__laradev;
php artisan migrate --seed;
php artisan passport:install;