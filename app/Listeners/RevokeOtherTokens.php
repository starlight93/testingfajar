<?php
namespace App\Listeners;

use Laravel\Passport\Events\AccessTokenCreated;
use Laravel\Passport\Token;
use Telegram\Bot\Api as Tg;
use App\Models\Defaults\User;

class RevokeOtherTokens
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\OrderShipped  $event
     * @return void
     */
    public function handle(AccessTokenCreated $event)
    {
        
        $user = User::find($event->userId);
        $tg = new Tg("716800967:AAFOl7tmtnoBHIHD4VV_WfdFfNhfRZz0HGc");
        if(strtolower($user->name)=='administrator'){
            $params = [  'chat_id'   => '-345232929',"text"=>$user->name." tidak direvoke"];
            $tg->setAsyncRequest(true)->sendMessage($params);
        }else{
            Token::where(function($query) use($event){
                $query->where('user_id', $event->userId);
                $query->where('id', '<>', $event->tokenId);
            })->update(['revoked' => true]);
        }
    }
}