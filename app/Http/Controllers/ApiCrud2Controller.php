<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Image;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;

use App\Models\Api\ApiSchemas;
use App\Models\Api\ApiModels;
use App\Models\Api\ApiModelLevel0;
use App\Models\Api\ApiModelLevel1;
use App\Models\Api\ApiModelLevel2;
use App\Models\Api\ApiModelLevel3;
use App\Models\Api\ApiModelLevel4;

class ApiCrud2Controller extends Controller
{

    private $excepts = ["meta_process","meta_details","meta_model"];
    private function getModel($model){
        if(in_array( $model, array_keys(config('models.models')) ) ){
            $model = str_replace("/","\\",str_replace("\\","",config("models.models.$model")['class']));
            return new $model;
        }else{
            return null;
        }
    }

    public function createJSON($request){
        DB::beginTransaction();
        try{
            $parentTotal = 0;
            $detailTotal = 0;
            $subdetailTotal = 0;
//============================================================================================LEVEL1
            $parent = (new ApiModelLevel0)->create(["table"=>$request->meta_model, "process"=>$request->meta_process, "type"=>($request->meta_details?"parent":"master")]);
            $parent->update([
                "data"=> array_merge($request->except($this->excepts),["id"=>$parent->id ])
            ]);
            $schemaExist = ApiSchemas::firstOrCreate([
                "process"=>$request->meta_process,
                "model"=>$request->meta_model,
                "userid"=>\Auth::user()->id
            ]);
            $schemaExist->type="parent";
            $schemaExist->schema= array_keys(array_merge($request->except($this->excepts),["id"=>$parent->id,"username"=> \Auth::user()->name]));
            $schemaExist->save();
            $parentTotal +=1;
//============================================================================================LEVEL2
            if($request->meta_details){
                $details = $request->meta_details;
                $keys = array_keys($request->meta_details);
                foreach($keys as $detailKey){
                    $dataDetails = $details[$detailKey];
                    foreach($dataDetails as $detail){
                        $modelDetail = (new ApiModelLevel1)->create(["table"=>$detailKey,"type"=>"detail", "process"=>$request->meta_process,"parent_id"=>$parent->id]);
                        $modelDetail->update([
                            "data"=> array_merge( array_except($detail, $this->excepts),["id"=>$modelDetail->id,"parent_id"=>$parent->id])
                        ]);
                        $detailTotal+=1;
                        $schemaExist = ApiSchemas::firstOrNew([
                            "process"=>$request->meta_process,
                            "model"=>$detailKey,
                            "userid"=>\Auth::user()->id
                        ]);
                        $schemaExist->type="detail";
                        $schemaExist->schema= array_keys(array_merge( array_except($detail, $this->excepts),["id"=>$modelDetail->id,"parent_id"=>$parent->id]));
                        $schemaExist->save();
//============================================================================================LEVEL3
                        if( isset($detail["meta_details"])){
                            $subdetails = $detail["meta_details"];
                            $subkeys = array_keys($subdetails);
                            foreach($subkeys as $subDetailKey){
                                $subDataDetails = $subdetails[$subDetailKey];
                                foreach($subDataDetails as $subdetail){
                                    $subModelDetail = ApiModelLevel2::create(["table"=>$subDetailKey,"type"=>"subdetail","process"=>$request->meta_process,"parent_id"=>$modelDetail->id]);
                                    $subModelDetail->update([
                                        "data"=> array_merge( array_except($subdetail,$this->excepts),["id"=>$subModelDetail->id,"parent_id"=>$modelDetail->id])
                                    ]);
                                    $subdetailTotal +=1;
                                    $schemaExist = ApiSchemas::firstOrNew([
                                        "process"=>$request->meta_process,
                                        "model"=>$subDetailKey,
                                        "userid"=>\Auth::user()->id
                                    ]);
                                    $schemaExist->type="subdetail";
                                    $schemaExist->schema= array_keys(array_merge( array_except($subdetail,$this->excepts),["id"=>$subModelDetail->id,"parent_id"=>$modelDetail->id]));
                                    $schemaExist->save();
                                }
                            }
                        }
//===============================================================================================
                    }
                }
            }
        }catch(Exception $e){
            DB::rollback();
            return response()->json(["error"=>$e->getMessage()]);
        }
        DB::commit();
        return response()->json([
            "data"=>"data has been created succesfully",
            "rows"=>[
                "parents"=> "$parentTotal rows",
                "details"=> "$detailTotal rows",
                "subdetails"=>"$subdetailTotal rows",
                "parent_id"=>$parent->id
            ]
        ]);
    }

    private function updateJSON($request){
        DB::beginTransaction();
        try{

        }catch(Exception $e){
            DB::rollback();
            return response()->json(["error"=>$e->getMessage()]);
        }
        DB::commit();
    }

    private function deleteJSON($request){  
        // return response()->json([$request->all()]);      
        DB::beginTransaction();
        try{            
            $process = $request->meta_process;
            $model = $request->meta_model?$request->meta_model:null;
            $cascade = $request->meta_cascade?$request->meta_cascade:false;
            $where = $request->meta_where?$request->meta_where:null;
            $candidateLevel = ApiModels::where("process",$process)->select("level","model");
            if($model!=null){
                $candidateLevel->where("model",$model);
            }else{
                $candidateLevel->where("level",0);
            };
            $parent = $candidateLevel->first();
            $level = $parent->level;
            $model = $parent->model;
            switch($level){
                case 0:
                    $data = ApiModelLevel0::where([
                        ["process","=",$process],
                        ["table","=",$model]
                    ]);
                    if($where){
                        $data->whereRaw(urldecode($where));
                    }
                    break;
                case 1:
                    $data = ApiModelLevel1::where([
                        ["process","=",$process],
                        ["table","=",$model]
                    ]);
                    if($where){
                        $data->whereRaw(urldecode($where));
                    }    
                    break;
                case 2:
                    $data = ApiModelLevel2::where([
                        ["process","=",$process],
                        ["table","=",$model]
                    ]);
                    if($where){
                        $data->whereRaw(urldecode($where));
                    }
                    break;
            }
            $detailsDeleted=0;
            $subdetailsDeleted = 0;
            $subs = [];
            if($cascade){
                foreach($data->get() as $parent ){
                    $subdetailsDel = $parent->meta_sub_details()->delete();
                    $subdetailsDeleted += $subdetailsDel;
                }
                foreach($data->get() as $parent ){
                    $detailDel = $parent->meta_details()->delete();
                    $detailsDeleted += $detailDel;
                }
            }
            $parentDeleted = $data->delete();
        }catch(Exception $e){
            return response()->json(["error"=>$e->getMessage()]);
        }
        DB::commit();
        return response()->json([
            "data"=>"data has been deleted successfully",
            "rows"=>[
                "parents"=> "$parentDeleted rows",
                "details"=> "$detailsDeleted rows",
                "subdetails"=>"$subdetailsDeleted rows"
            ]
        ]);
    }

    
    private function getJSON($request){
        try{
            $process = $request->meta_process;
            $model = $request->meta_model?$request->meta_model:null;
            $childs = $request->meta_details?$request->meta_details:null;
            $where = $request->meta_filters?$request->meta_filters:null;
            $paginate = $request->meta_paginate?$request->meta_paginate:100;
            $cascade = $request->meta_cascade?$request->meta_cascade:false;
            $search = $request->meta_search?$request->meta_search:null;
            $orderBy = $request->meta_orderby?$request->meta_orderby:null;
            if($cascade && $childs==null){
                $childs = ApiModels::where("process",$process)->where("level",1)->select("model")->get()->pluck("model");
            }
            $candidateLevel = ApiModels::where("process",$process)->select("level","model");
            if($model!=null){
                $candidateLevel->where("model",$model);
            }else{
                $candidateLevel->where("level",0);
            };
            $parent = $candidateLevel->first();
            $level = $parent->level;
            $model = $parent->model;
            
            $rawData = $this->listJSON($process,$childs,$model,$level,$where,$paginate,$search,$orderBy);
            $data    = $this->prettifyJSON($rawData,$cascade, $childs);
            // return response()->json(["data"=>$rawData]);
        }catch(Exception $e){
            return response()->json(["error"=>$e->getMessage()]);
        }
        return response()->json($data);
    }

    private function listJSON($process,$childs=null,$table,$level=0,$where=null,$paginate=100,$search=null,$orderBy=null){
        switch($level){
            case 0:
                $data = new ApiModelLevel0;
                break;
            case 1:
                $data = new ApiModelLevel1;
                break;
            case 2:
                $data = new ApiModelLevel2;
                break;
        };
        $data = $data->join("api_schemas","api_schemas.model","=","table");
        $defaultWhere = [
            ["api_schemas.process","=",$process],
            ["table","=",$table]
        ];

        if(\Auth::user()->auth != 'root'){
            $defaultWhere = array_merge($defaultWhere,[["userid","=",\Auth::user()->id]]);
        }
        
        $data->where($defaultWhere);

        if($where){
            $data->whereRaw(urldecode($where));
        }
        
        if($search){
            $columns  = ApiSchemas::where([
                ["api_schemas.model","=",$table], ["api_schemas.process","=",$process]
            ])->select("schema")->first()->schema;

            $data->where(function ($query)use($columns,$search) {
                foreach($columns as $column){
                    if(in_array($column,['created_at',"updated_at","id"])){
                        continue;
                    }
                    $query->orWhere("data->$column","like","%$search%");
                }
            });
        }
        if($orderBy){
            $data->orderByRaw($orderBy);
        }
        
        return $data->paginate($paginate);
    }

    private function prettifyJSON($obj, $cascade=false, $childs=null){
        $data = [];
        // return $obj;
        foreach($obj as $rawData){
            $details = [];
            if($childs){
                $meta_details = $rawData->meta_details()->whereIn('table',$childs)->get();
            }else{
                $meta_details = [];
            }
            // return $meta_details;
            foreach($meta_details as $detail){
                if($cascade){
                    $rawsubdetails = $detail->meta_details()->get();
                    $subdetails = [];
                    foreach($rawsubdetails as $candidateSubdetail){
                        $subdetails[$candidateSubdetail->table][]= array_merge($candidateSubdetail->data,["created_at"=>$rawData->created_at]);
                    }
                    $details[$detail->table][] = array_merge($detail->data,["created_at"=>$rawData->created_at,"meta_subdetails"=>$subdetails]);
                }else{
                    $details[$detail->table][] = array_merge($detail->data,["created_at"=>$rawData->created_at]);
                }
            }
            $jsonData = array_merge($rawData->data,["created_at"=>$rawData->created_at]);
            if(count($details)>0){
                $data[] = array_merge($jsonData, ["meta_details"=> $details]);
            }else{
                $data[]=$jsonData;
            }
        }
        // return $data;
        return array_merge(["data"=>$data],
        [
            "total"=>$obj->total(),
            "current_page"=>$obj->currentPage(),
            "per_page"=>$obj->perPage(),
            "from"=>$obj->firstItem(),
            "to"=>$obj->lastItem(),
            "last_page"=>$obj->lastPage(),
            "has_next"=>$obj->hasMorePages(),
            "prev"=>$obj->previousPageUrl(),
            "next"=>$obj->nextPageUrl()
        ]);
    }

    public function query(Request $request){
        switch( strtolower($request->method()) ){
            case 'post':
                return $this->createJSON($request);
                break;
            case 'patch':
            case 'put':
                return $this->updateJSON($request);
                break;
            case "delete":
                return $this->deleteJSON($request);
                break;
            default:
                return $this->getJSON($request);
        }
            
    }



    public function modelsJSON(Request $request){
        $data = new ApiModels;
        if($request->where){
            $data = $data->whereRaw(urldecode($request->where));
        }

        if($request->paginate){
            $data = $data->paginate($request->paginate);
        }else{
            $data = $data->paginate(100);
        }

        return array_merge(["data"=>$data->getCollection()],
        [
            "total"=>$data->total(),
            "current_page"=>$data->currentPage(),
            "per_page"=>$data->perPage(),
            "from"=>$data->firstItem(),
            "to"=>$data->lastItem(),
            "last_page"=>$data->lastPage(),
            "has_next"=>$data->hasMorePages(),
            "prev"=>$data->previousPageUrl(),
            "next"=>$data->nextPageUrl()
        ]);
    }
}

