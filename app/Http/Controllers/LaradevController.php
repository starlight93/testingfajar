<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class LaradevController extends Controller
{
    private $base_model;
    public function __construct()
    {
        //
    }
    private function getModel($modelName){
        switch($modelName){
            case "forms": 
                return (new \App\Models\Laradev\LaradevForm);
                break;            
            case "comments": 
                return new \App\Models\Laradev\LaradevFormComments;
                break;        
            case "chats": 
                return new \App\Models\Laradev\LaradevChats;
                break;            
            case "notifications": 
                return new \App\Models\Laradev\LaradevNotifications;
                break;
            case "api": 
                return new \App\Models\Laradev\LaradevApi;
                break;
            case "dbconnections": 
                return new \App\Models\Laradev\LaradevDBConnections;
                break;
            case "servers": 
                return new \App\Models\Laradev\LaradevServerList;
                break;
            case "tables": 
                return new \App\Models\Laradev\LaradevTables;
                break;
            default: return null;                                    
        }
    }

    private function create($request, $model){
        $request->merge([
            "user_id"=>\Auth::user()->id    
        ]);
        $availableColumns = Schema::getColumnListing($this->base_model->getTable());
        $requestColumns = array_keys($request->except(['id','created_at','updated_at']));
        $acceptedColumns = [];
        $droppedColumns = [];
        foreach($requestColumns as $col){
            if(in_array($col, $availableColumns)){
                $acceptedColumns[]=$col;
            }else{
                $droppedColumns[]=$col;
            }
        }
        // return $request->only(['rowname']);
        $result = count($acceptedColumns)>0?$model->create($request->only($acceptedColumns)):null;
        if($result && in_array($model->getTable(),["laradev_forms_comments","laradev_chats"])){
            if($model->getTable()=='laradev_forms_comments'){
                $form = \App\Models\Laradev\LaradevForm::find($request->laradev_forms_id);
                // DB::connection("sqlite")->table("notifications")->insert([
                //     "recipient_type" => "individual",
                //     "recipient_target" => $form->user_id,
                //     "recipient_data"=>"notification"
                // ]);
            }elseif($model->getTable()=='laradev_chats'){
                // DB::connection("sqlite")->table("notifications")->insert([
                //     "recipient_type" => $result->type,
                //     "recipient_target" => $result->recipient_id,
                //     "recipient_data"=>"chats"
                // ]);
            }
        }
        return response()->json(['data'=>$result, 'dropped'=>$droppedColumns,'accepted'=>$acceptedColumns],$result?201:422);
    }

    private function update($request, $model, $id){
        $availableColumns = Schema::getColumnListing($this->base_model->getTable());
        $requestColumns = array_keys($request->except(['id','created_at','updated_at']));
        $acceptedColumns = [];
        $droppedColumns = [];
        foreach($requestColumns as $col){
            if(in_array($col, $availableColumns)){
                $acceptedColumns[]=$col;
            }else{
                $droppedColumns[]=$col;
            }
        }      
        
        $result = count($acceptedColumns)>0?$model->find($id)->update($request->only($acceptedColumns)):null;
        
        return response()->json(['data'=>$result,"id"=>$id, 'dropped'=>$droppedColumns,'accepted'=>$acceptedColumns],$result?200:422);
    }

    private function delete($request, $model, $id){
        $result = $model->find($id)->delete();
        return response()->json(['data'=>$result]);
    }

    private function read($request, $model, $id){
        if($id){
            $result = $model
                    ->find($id);
            return response()->json(['data'=>$result]);
        }
        if(\Auth::user()->auth == 'frontend'){
            $model = $model->where("user_id",\Auth::user()->id);
        }
        if($request->where){
            $model = $model->whereRaw( urldecode($request->where) );
        }
        $result = $model->leftJoin('default_users','default_users.id','=', $this->base_model->getTable().".user_id")
                ->select($this->base_model->getTable().".*","default_users.name")
                ->orderBy('updated_at','desc')
                ->get();
        return response()->json(['data'=>$result]);
    }

    public function index(Request $request, $model, $id=null){
        $modelName=$model;
        $model = $this->getModel($model);
        $this->base_model=$model;
        switch( strtolower($request->method()) ){
            case 'post':
                return $this->create($request, $model);
                break;
            case 'patch':
            case 'put':
                return $this->update($request, $model, $id);
                break;
            case "delete":
                return $this->delete($request, $model, $id);
                break;
            default:
                if($modelName=='forms'){
                    $model=$model->with("comments");
                }
                return $this->read($request, $model, $id);
        }
    }
    public function settings(Request $request,$data){
        $table = strtolower($data);
        $list  = DB::table("laradev_settings")->where('type',$data)->select("data")->get()->pluck("data");
        return $list;
    }
    public function allSettings(Request $request){
        $data  = DB::table("laradev_settings")->select("data","type")->orderBy("data","asc")->get();
        $settings = [];
        foreach($data as $isi){
            $settings[$isi->type][] = $isi->data;
        }
        $connections = DB::table("laradev_dbconnections")->select("name")->pluck("name");
        $settings["dbconnections"] = $connections;
        return $settings;
    }
    public function getNotifications(Request $request){
        $notifications  = \App\Models\Laradev\LaradevNotifications::where("user_id",\Auth::user()->id)->orWhere("user_id",-1);
        if($request->where){
            $notifications->$notifications->whereRaw(urlencode($request->where));
        }
        $notifications=$notifications->orderBy('id',"desc")->get();
        return $notifications;
    }
    public function getChats(Request $request){
        $chats  = \App\Models\Laradev\LaradevChats::where("user_id",\Auth::user()->id)->orWhere("user_id",-1)->orWhere("recipient_id",\Auth::user()->id)->get();
        return response()->json($chats);
    }
}
