<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Image;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;

class CustomController extends Controller
{
    public function query(Request $request, $project, $table, $id=null){
        switch($request->method()){
            case 'GET':
                return $this->read($request, $project."_$table", $id);
                break;
            case 'PUT':
            case 'PATCH':
                return $this->read($request, $project."_$table", $id);
                break;
            case 'DELETE':
                return $this->delete($request, $project."_$table", $id);
                break;
            case 'POST':
                return $this->create($request, $project."_$table");
                break ;
        }
    }

    private function read($request, $table, $id){
        $data = DB::table($table);
        if($id!=null){
            return $data->where("id",$id)->get();
        }
        if($request->where){
            $data = $data->whereRaw(urldecode($request->where));
        }
        
        if($request->rawquery){
            $data = $data->select(DB::raw(urldecode($request->rawquery)));
        }
        if($request->filter){
            $key=$request->filter;
            $columns =  array_filter((Schema::getColumnListing($table)),function($col){
                if(!in_array($col,["created_at","updated_at","user_id","id","created_by"]) ){
                    return $col;
                }
            });
            $data=$data->where("id",">",0);
            $data = $data->where(function($query)use($key,$columns){
                foreach($columns as $column){
                    $query->orWhere($column,"LIKE","%$key%");
                };
            });
        }
        if($request->groupby){
            $data->groupBy(DB::raw(urldecode($request->groupby) ));
        }
        if($request->orderby){
            $data->orderBy($request->orderby,$request->ordertype==null?"asc":$request->ordertype);
        }
        
        if($request->raworderby){
            $data->orderByRaw($request->raworderby );
        }
        $data = $data->paginate($request->paginate==null?100:$request->paginate);
        if($request->isJson){
            $array = [];
            foreach($data->pluck('data') as $mydata){
                $array[]=json_decode($mydata,true);
            }
            return response()->json(array_merge(["data"=>$array],
            [
                "total"=>$data->total(),
                "current_page"=>$data->currentPage(),
                "per_page"=>$data->perPage(),
                "from"=>$data->firstItem(),
                "to"=>$data->lastItem(),
                "last_page"=>$data->lastPage(),
                "has_next"=>$data->hasMorePages(),
                "prev"=>$data->previousPageUrl(),
                "next"=>$data->nextPageUrl()
            ]));
        }
        return $data;
    }
    private function update($request, $table, $id){
        $request->merge([
            "created_at" => date("now"),
            "updated_at" => date("now")
        ]);
        $data = DB::table($table);
        $data = DB::table($table)->where("id",$id)->update($request->except(["id"]));
        return response()->json([
            "data"=> "Delete on $table successfully id=$id"
        ]);
    }
    private function delete($request, $table, $id){        
        $data = DB::table($table)->where("id",$id)->delete();   
        return response()->json([
            "data"=> "Delete on $table successfully id=$id"
        ]);
    }
    private function create($request, $table){
        $data = DB::table($table)->insertGetId($request->except(['id']));        
        return response()->json([
            "data"=> "Created on $table successfully id=$data"
        ]);            
    }
    
}

