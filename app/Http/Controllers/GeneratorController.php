<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class GeneratorController extends Controller
{
    private $framePath = "";
    public function __construct()
    {
        $this->framePath = base_path("generators/");
    }

    private function getBasicModelFrame(){
        $data = File::get($this->framePath."basicModel.php");
        return $data;

    }
    private function getCustomModelFrame(){
        $data = File::get($this->framePath."customModel.php");
        return $data;

    }
    public function index(Request $request, $query){
        switch($query){
            case "get-basic-model-frame": 
                return $this->getBasicModelFrame();
                break;
            case "get-custom-model-frame": 
                return $this->getCustomModelFrame();
                break;
            default: 
                return response()->json(["status"=>"query salah"],422);
        }
    }
}
