<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class NonApiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function sample(Request $request){
        return view('defaults.sample');
    }
    public function resources(Request $request){
        return view('defaults.resources');
    }
}