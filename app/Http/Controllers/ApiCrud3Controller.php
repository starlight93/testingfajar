<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Image;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;

use App\Models\Resources\Models;
use App\Models\Resources\Schemas;
use App\Models\Resources\ModelLevel0;
use App\Models\Resources\ModelLevel1;
use App\Models\Resources\ModelLevel2;

class ApiCrud3Controller extends Controller
{
    private $isPublic=false;

    private function createSchema($model, $schema){
        $hasil = $model->schemas()->first();
        if(!$hasil){
            $model->schemas()->create($schema);
        }else{
            $hasil->update($schema);
        }
        return true;
    }

    private function getReaderModel($project, $modelName){
        $where =[
             "project" => strtolower($project),
             "model"   => strtolower($modelName)
        ];
        if(!$this->isPublic){
            $where = array_merge($where, [ "user_id" => \Auth::user()->id]);
        }
        $model = Models::where($where)->first();
        return $model;
    }

    private function getCreatorModel($project, $modelName, $level=0){
        $model = Models::firstOrCreate([
            "project"   =>  strtolower($project),
            "model"     =>  strtolower($modelName),
            "level"     =>  $level,
            "is_public" =>  $this->isPublic?1:0,
            "user_id"   =>  \Auth::user()->id
        ]);
        return $model;
    }

    public function createJSON($request, $project, $modelName){
        DB::beginTransaction();
        try{
            $parentTotal = 0;
            $detailTotal = 0;
            $subDetailTotal = 0;

            $parentSchema = [];
            $detailSchema = [];
            $subDetailSchema = [];
            foreach(array_keys($request->all()) as $key){
                if (strpos($key, '_detail') !== false && is_array( ($request->$key) ) && count($request->$key)>0 ) {
                    foreach( array_keys( ($request->$key) [0]) as $detailKey ){
                        if (strpos($detailKey, '_detail') !== false && is_array((($request->$key)[0])[$detailKey]) ) {
                            $subDetailSchema[$detailKey] =  array_keys((($request->$key)[0])[$detailKey][0]) ;
                        }else{
                            $detailSchema[$key][] = $detailKey;
                        }
                    }

                }else{
                    $parentSchema[] = $key;
                }
            }
            $parentModel = $this->getCreatorModel($project, $modelName, 0);

            $this->createSchema($parentModel, [
                "level"     =>  0,
                "schema"    =>  $parentSchema
            ]);
            $parent_data = $request->except(array_keys($detailSchema));
            $array_model = explode("_",$modelName);
            if(in_array("tra",$array_model)){
                $parent_data = array_merge($parent_data,["transaction_no"=>($request->draft_no?$request->draft_no:crc32(uniqid())) ]);
            }
            $createdParent = $parentModel->level0()->create(['data'=>$parent_data]);
            $parentTotal+=1;
            
            $detail = [];
            foreach($detailSchema as $detailName=> $schema){
                $detailModel = $this->getCreatorModel($project, $detailName, 1);
                $this->createSchema($detailModel,[
                    "level"     =>  1,
                    "schema"    =>  $schema
                ]);
                foreach($request->$detailName as $detailData){
                    $createdDetail = $detailModel->level1()->create( array_merge([ 'data'=>array_except($detailData,array_keys($subDetailSchema))], ['parent_id'=>$createdParent->id]) );
                    $detailTotal+=1;
                    $subDetails = array_intersect_key($detailData, $subDetailSchema);
                    foreach($subDetails as $key => $values){
                        $subDetailModel = $this->getCreatorModel($project, $key, 2);
                        $this->createSchema($subDetailModel,[
                            "level"     =>  2,
                            "schema"    =>  $subDetailSchema[$key]
                        ]);
                        foreach($values as $detailValue){
                            $subDetailModel->level2()->create( array_merge([ 'data'=>$detailValue], ['parent_id'=>$createdDetail->id]) );
                            $subDetailTotal+=1;
                        }
                    }
                }
            }
        }catch(Exception $e){
            DB::rollback();
            // logTg("laradev", (\Auth::user()->name)." created data $modelName FAILED :".$e->getMessage());
            return response()->json(["error"=>$e->getMessage()]);
        }
        DB::commit();
        // logTg("laradev", (\Auth::user()->name)." created data $modelName successfully");
        return response()->json([
            "data"=> [
                "id"    => $createdParent->id,
                "rows"=>[
                    "parents"=> "$parentTotal rows",
                    "details"=> "$detailTotal rows",
                    "subdetails"=>"$subDetailTotal rows"
                ]
            ],
            "info" => "data has been created succesfully"
        ]);
    }

    private function updateJSON($request, $project, $modelName, $id=null){
        DB::beginTransaction();
        try{
            if(strtolower($request->method()) == 'patch' && $id!=null){
                $data = ModelLevel0::find($id);
                $data->update([
                    'data'=>array_merge($data->data,["status"=>strtoupper($request->status)])
                ]);
                return response()->json([
                    "data"=> [
                        "id"    => $id
                    ],
                    "info" => "data has been updated succesfully"
                ]);
            }
            $parentTotal = 0;
            $detailTotal = 0;
            $subDetailTotal = 0;

            $parentSchema = [];
            $detailSchema = [];
            $subDetailSchema = [];

            foreach(array_keys($request->all()) as $key){
                if (strpos($key, '_detail') !== false && is_array( ($request->$key) ) && count($request->$key)>0 ) {
                    foreach( array_keys( ($request->$key) [0]) as $detailKey ){
                        if (strpos($detailKey, '_detail') !== false && is_array((($request->$key)[0])[$detailKey])) {
                            $subDetailSchema[$detailKey] =  array_keys((($request->$key)[0])[$detailKey][0]) ;
                        }else{
                            $detailSchema[$key][] = $detailKey;
                        }
                    }
                }else{
                    $parentSchema[] = $key;
                }
            }
            $parentModel = $this->getReaderModel($project, $modelName);

            // $data = ModelLevel0::where('model_id',$parentModel->id)->where('id', $id)->first();
            $data = ModelLevel0::where('id', $id)->first();
            if(!$data){
                return response()->json([
                    "data"=> [
                        "rows"=>[
                            "parents"=> "0 rows",
                            "details"=> "0 rows",
                            "subdetails"=>"0 rows"
                        ]
                    ],
                    "info" => "0 data has been updated"
                ]);
            }
            if($parentModel){
                $this->createSchema($parentModel, [
                    "level"     =>  0,
                    "schema"    =>  $parentSchema
                ]);
            }

            $subDetailsDeleted  = $data->subDetails()->delete();
            $detailsDeleted     = $data->details()->delete();
            $createdParent = $data->update(['data'=>$request->except(array_keys($detailSchema))]);
            $parentTotal+=1;
            
            $detail = [];
            foreach($detailSchema as $detailName=> $schema){
                $detailModel = $this->getCreatorModel($project, $detailName, 1);
                $this->createSchema($detailModel,[
                    "level"     =>  1,
                    "schema"    =>  $schema
                ]);
                foreach($request->$detailName as $detailData){
                    $createdDetail = $detailModel->level1()->create( array_merge([ 'data'=>array_except($detailData,array_keys($subDetailSchema))], ['parent_id'=>$id]) );
                    $detailTotal+=1;
                    $subDetails = array_intersect_key($detailData, $subDetailSchema);
                    foreach($subDetails as $key => $values){
                        $subDetailModel = $this->getCreatorModel($project, $key, 2);
                        $this->createSchema($subDetailModel,[
                            "level"     =>  2,
                            "schema"    =>  $subDetailSchema[$key]
                        ]);
                        foreach($values as $detailValue){
                            $subDetailModel->level2()->create( array_merge([ 'data'=>$detailValue], ['parent_id'=>$createdDetail->id]) );
                            $subDetailTotal+=1;
                        }
                    }
                }
            }
        }catch(Exception $e){
            DB::rollback();
            // logTg("laradev", (\Auth::user()->name)." updated data $modelName id=$id FAILED :".$e->getMessage());
            return response()->json(["error"=>$e->getMessage()]);
        }
        DB::commit();
        // logTg("laradev", (\Auth::user()->name)." updated data $modelName id=$id successfully");
        return response()->json([
            "data"=> [
                "id"    => $id,
                "rows"=>[
                    "parents"=> "$parentTotal rows",
                    "details"=> "$detailTotal rows",
                    "subdetails"=>"$subDetailTotal rows"
                ]
            ],
            "info" => "data has been updated succesfully"
        ]);
    }

    private function deleteJSON($request, $project, $modelName, $id=null){ 
        DB::beginTransaction();
        try{
            $model = $this->getReaderModel($project, $modelName);
            $data = ModelLevel0::where('model_id',$model->id)->where('id', $id)->first();
            if(!$data){
                return response()->json([
                    "data"=> [
                        "rows"=>[
                            "parents"=> "0 rows",
                            "details"=> "0 rows",
                            "subdetails"=>"0 rows"
                        ]
                    ],
                    "info" => "0 data has been deleted"
                ]);
            }
            $subDetailsDeleted  = $data->subDetails()->delete();
            $detailsDeleted     = $data->details()->delete();
            $parentDeleted      = $data->delete();
        }catch(Exception $e){
            logTg("laradev", (\Auth::user()->name)." deleted data $modelName  id=$id FAILED :".$e->getMessage());
            return response()->json(["error"=>$e->getMessage()]);
        }
        DB::commit();
        // logTg("laradev", (\Auth::user()->name)." deleted data $modelName  id=$id successfully");
        return response()->json([
            "data"=> [
                "rows"=>[
                    "parents"=> "$parentDeleted rows",
                    "details"=> "$detailsDeleted rows",
                    "subdetails"=>"$subDetailsDeleted rows"
                ]
            ],
            "info" => "data has been deleted successfully"
        ]);
    }

    private function readJSON($request, $project, $modelName, $id=null){
        try{
            $model = $this->getReaderModel($project, $modelName);
            if(!$model){
                return response()->json(["info"=>"resource was not found","data"=>[]]);
            }
            if($id==null){
                $data = ModelLevel0::join("res_models","res_models.id","=","res_model_0.model_id")
                        ->selectRaw("res_model_0.id,JSON_MERGE_PATCH(res_model_0.data, JSON_OBJECT('id',res_model_0.id, 'updated_at', date(res_model_0.updated_at) )) as data")
                        ->where('model_id',$model->id);
                
                
                if($request->where){
                    $data = $data->whereRaw(urldecode($request->where));
                }
                if($request->filter){
                    $key=$request->filter;

                    $columns =  Schemas::select("schema")->where("model_id",$model->id)->get()->pluck("schema")[0];
                    // return $columns;
                    $data=$data->where("res_model_0.id",">",0);
                    $data = $data->where(function($query)use($key,$columns){
                        foreach($columns as $column){
                            $query->orWhere("data->$column","LIKE","%$key%");
                        };
                    });
                }
                // return $data->toSql();
                // return is_numeric("100.0000,00")?"ya":"no";
                if($request->orderby){
                    $order = $request->orderby;
                    if(in_array($order,["id","created_at","updated_at","user_id"])){
                        $order = "res_model_0.$order";
                    }else{
                        $order = "data->$order";
                    }
                    $data=$data->orderBy($order,$request->ordertype==null?"asc":$request->ordertype);
                }
                if($request->raworderby){
                    $data=$data->orderByRaw(urlencode($request->raworderby) );
                }
                $data = $data->paginate($request->paginate==null?100:$request->paginate);
                
                // logTg("laradev", (\Auth::user()->name)." read data $modelName successfully");
                return response()->json(array_merge(["data"=>$data->pluck('data')],
                [
                    "total"=>$data->total(),
                    "current_page"=>$data->currentPage(),
                    "per_page"=>$data->perPage(),
                    "from"=>$data->firstItem(),
                    "to"=>$data->lastItem(),
                    "last_page"=>$data->lastPage(),
                    "has_next"=>$data->hasMorePages(),
                    "prev"=>$data->previousPageUrl(),
                    "next"=>$data->nextPageUrl()
                ]));
            };

            $data = ModelLevel0::with(['details' => function ($query) {
                $query->join("res_models","res_models.id","=","res_model_1.model_id");
                $query->selectRaw("res_model_1.id, res_model_1.parent_id, res_models.model, JSON_MERGE_PATCH(data, JSON_OBJECT('id',res_model_1.id, 'updated_at', date(res_model_1.updated_at) ) ) as data");
                $query->with(["subDetails"=>function($q){
                    $q->selectRaw("res_model_2.id, res_model_2.parent_id, res_models.model, JSON_MERGE_PATCH(data, JSON_OBJECT('id',res_model_2.id, 'updated_at', date(res_model_2.updated_at) ) ) as data");
                    $q->join("res_models","res_models.id","=","res_model_2.model_id");
                }]);
            }])->selectRaw("id,JSON_MERGE_PATCH(data, JSON_OBJECT('id',id, 'updated_at', date(updated_at) ) ) as data")->find($id);
            // ->where('model_id',$model->id)->where('id', $id)->first();
            $data = $this->__format($data);
            // logTg("laradev", (\Auth::user()->name)." read data $modelName id=$id successfully");
        }catch(Exception $e){
            // logTg("laradev", (\Auth::user()->name)." read data $modelName FAILED :".$e->getMessage());
            return response()->json(["error"=>$e->getMessage()]);
        }
        
        return response()->json($data);
    }

    private function readJSONDetail($request, $project, $modelName, $id=null){
        $model = $this->getReaderModel($project, $modelName);
        if(!$model){
            return response()->json(["info"=>"resource was not found","data"=>[]]);
        }
            $data = ModelLevel1::join("res_models","res_models.id","=","res_model_1.model_id")
                ->join("res_model_0","res_model_0.id","=", "res_model_1.parent_id")
                ->selectRaw("res_model_1.id, JSON_MERGE_PATCH( res_model_0.data,res_model_1.data, JSON_OBJECT('parent_id',res_model_1.parent_id,'id',res_model_1.id, 'res_model_1.updated_at', date(res_model_1.updated_at) )) as data")
                ->where('res_model_1.model_id',$model->id);
            if($request->parent_id){
                $data= $data->where('res_model_1.parent_id',$request->parent_id);
            }
            if($request->where){
                $data = $data->whereRaw(urldecode($request->where));
            }
            if($request->filter){
                $key=$request->filter;

                $columns =  Schemas::select("schema")->where("model_id",$model->id)->get()->pluck("schema")[0];
                $data=$data->where("res_model_1.id",">",0);
                $data = $data->where(function($query)use($key,$columns){
                    foreach($columns as $column){
                        $query->orWhere("res_model_1.data->$column","LIKE","%$key%");
                    };
                });
            }
            if($request->orderby){
                $order = $request->orderby;
                if(in_array($order,["id","created_at","updated_at","user_id"])){
                    $order = "res_model_1.$order";
                }else{
                    $order = "res_model_1.data->$order";
                }
                $data=$data->orderBy($order,$request->ordertype==null?"asc":$request->ordertype);
            }
            if($request->raworderby){
                $data=$data->orderByRaw(urlencode($request->raworderby) );
            }
            $data = $data->paginate($request->paginate==null?100:$request->paginate);

            return response()->json(array_merge(["data"=>$data->pluck('data')],
            [
                "total"=>$data->total(),
                "current_page"=>$data->currentPage(),
                "per_page"=>$data->perPage(),
                "from"=>$data->firstItem(),
                "to"=>$data->lastItem(),
                "last_page"=>$data->lastPage(),
                "has_next"=>$data->hasMorePages(),
                "prev"=>$data->previousPageUrl(),
                "next"=>$data->nextPageUrl()
            ]));
    }
    private function readJSONSubDetail($request, $project, $modelName, $id=null){
        $model = $this->getReaderModel($project, str_replace("_of_detail","",$modelName) );
        if(!$model){
            return response()->json(["info"=>"resource was not found","data"=>[]]);
        }
            $data = ModelLevel2::join("res_models","res_models.id","=","res_model_2.model_id")
                ->join("res_model_1","res_model_1.id","=", "res_model_2.parent_id")
                ->selectRaw("res_model_2.id, JSON_MERGE_PATCH(res_model_1.data,res_model_2.data,  JSON_OBJECT('parent_id',res_model_2.parent_id,'id',res_model_2.id, 'res_model_2.updated_at', date(res_model_2.updated_at) )) as data")
                ->where('res_model_2.model_id',$model->id);
            if($request->parent_id){
                $data= $data->where('res_model_2.parent_id',$request->parent_id);
            }
            if($request->where){
                $data = $data->whereRaw(urldecode($request->where));
            }
            if($request->filter){
                $key=$request->filter;

                $columns =  Schemas::select("schema")->where("model_id",$model->id)->get()->pluck("schema")[0];
                $data=$data->where("res_model_2.id",">",0);
                $data = $data->where(function($query)use($key,$columns){
                    foreach($columns as $column){
                        $query->orWhere("res_model_2.data->$column","LIKE","%$key%");
                    };
                });
            }
            if($request->orderby){
                $order = $request->orderby;
                if(in_array($order,["id","created_at","updated_at","user_id"])){
                    $order = "res_model_2.$order";
                }else{
                    $order = "res_model_2.data->$order";
                }
                $data=$data->orderBy($order,$request->ordertype==null?"asc":$request->ordertype);
            }
            if($request->raworderby){
                $data=$data->orderByRaw(urlencode($request->raworderby) );
            }
            $data = $data->paginate($request->paginate==null?100:$request->paginate);

            return response()->json(array_merge(["data"=>$data->pluck('data')],
            [
                "total"=>$data->total(),
                "current_page"=>$data->currentPage(),
                "per_page"=>$data->perPage(),
                "from"=>$data->firstItem(),
                "to"=>$data->lastItem(),
                "last_page"=>$data->lastPage(),
                "has_next"=>$data->hasMorePages(),
                "prev"=>$data->previousPageUrl(),
                "next"=>$data->nextPageUrl()
            ]));
    }
    private function __format($data){
        $details = $data->details;
        $newDetail = [];
        foreach($details as $detail){
            $newSubDetail = [];
            $subDetails = $detail->subDetails;
            foreach($subDetails as $subDetail){
                $newSubDetail[$subDetail->model][] = $subDetail->data;
            }
            
            $newDetail[$detail->model][] = (object)array_merge($detail->data, $newSubDetail);
        }
        return array_merge($data->data,$newDetail);
    }
    
    public function query(Request $request, $project, $model, $id=null){
        if(explode("/",$request->path())[0] == "public-resources"){
            $this->isPublic = true;
        };
        switch( strtolower($request->method()) ){
            case 'post':
                return $this->createJSON($request, $project, $model);
                break;
            case 'patch':
            case 'put':
                return $this->updateJSON($request, $project, $model, $id);
                break;
            case "delete":
                return $this->deleteJSON($request, $project, $model, $id);
                break;
            default:
                if (strpos($model, '_of_detail') !== false){ 
                    return$this->readJSONSubDetail($request, $project, $model, $id);
                }elseif (strpos($model, '_detail') !== false){ 
                    return$this->readJSONDetail($request, $project, $model, $id);
                }else{
                    return $this->readJSON($request, $project, $model, $id);
                }
                
        }
            
    }

    public function modelsJSON(Request $request){
        $data = Models::with(["schemas"=>function($q){
            $q->select("schema","model_id");
        }])->select("res_models.id","project", "model", "default_users.name as owner", "res_models.updated_at")
                    ->join('default_users','default_users.id','res_models.user_id')
                    ->whereIn("level",[0,1]);
        if($request->where){
            $data = $data->whereRaw(urldecode($request->where));
        }
        if($request->orderby){
            $data->orderBy($request->orderby,$request->ordertype==null?"asc":$request->ordertype);
        }else{
            
            $data->orderBy('res_models.model','asc');
        }
        $data = $data->paginate($request->paginate==null?10000:$request->paginate);

        return response()->json(
            array_merge(
                [
                    "data"=>$data->getCollection()
                ],
                [
                    "total"=>$data->total(),
                    "current_page"=>$data->currentPage(),
                    "per_page"=>$data->perPage(),
                    "from"=>$data->firstItem(),
                    "to"=>$data->lastItem(),
                    "last_page"=>$data->lastPage(),
                    "has_next"=>$data->hasMorePages(),
                    "prev"=>$data->previousPageUrl(),
                    "next"=>$data->nextPageUrl()
                ]
            )
        );
    }
    public function updatepartial(Request $request, $project, $model, $id=null){
        $modelName = $model;
        $this->isPublic = true;
        DB::beginTransaction();
        try{
            $parentTotal = 0;
            $detailTotal = 0;
            $subDetailTotal = 0;

            $parentModel = $this->getReaderModel($project, $modelName);
            $data = ModelLevel0::where('model_id',$parentModel->id)->where('id', $id)->first();
            if(!$data){
                return response()->json([
                    "data"=> [
                        "rows"=>[
                            "parents"=> "0 rows",
                            "details"=> "0 rows",
                            "subdetails"=>"0 rows"
                        ]
                    ],
                    "info" => "0 data has been updated"
                ]);
            }

            $createdParent = $data->update(['data'=>array_merge($data->data,$request->all())]);
            $parentTotal+=1;
            
        }catch(Exception $e){
            DB::rollback();
            // logTg("laradev", (\Auth::user()->name)." updated data $modelName id=$id FAILED :".$e->getMessage());
            return response()->json(["error"=>$e->getMessage()]);
        }
        DB::commit();
        // logTg("laradev", (\Auth::user()->name)." updated data $modelName id=$id successfully");
        return response()->json([
            "data"=> [
                "id"    => $id,
                "rows"=>[
                    "parents"=> "$parentTotal rows",
                    "details"=> "$detailTotal rows",
                    "subdetails"=>"$subDetailTotal rows"
                ]
            ],
            "info" => "data has been updated succesfully"
        ]);
    }
}

