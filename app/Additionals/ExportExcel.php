<?php
namespace App\Additionals;

use App\Models\Transaction\CsmsDetails;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ExportExcel implements FromView, ShouldAutoSize
{
    private $data;
    // protected $id;
    public function __construct($data)
    {
        $this->data = $data; 
    }
    // public function collection()
    // {
    //     $detail = CsmsDetails::where('supplier_csms_id',$this->id)
    //             ->join('m_csms_questions',function( $join ){
    //                 $join->on('m_csms_questions.code', '=', 'm_supplier_csms_detail.code');
    //                 $join->on('m_csms_questions.seq_number', '=', 'm_supplier_csms_detail.seq_number');
    //             })
    //             ->select(DB::raw("m_csms_questions.id,m_csms_questions.title,m_csms_questions.question, m_supplier_csms_detail.answer, CONCAT('".url("/uploads")."/',m_supplier_csms_detail.url),'belum dicheck' AS checked "))
    //             ->get();
    //     return $detail;
    // }
    // public function headings(): array
    // {
    //     return [
    //         'No',
    //         'Title',
    //         'Question',
    //         'Answer',
    //         'Alamat File',
    //         'Checked?'
    //     ];
    // }
    public function view(): View
    {
        $data=$this->data;
        return view('defaults.pdf', compact('data'));
    }
}
