<?php

namespace App\Models\Api;

use Illuminate\Database\Eloquent\Model;

class ApiModelLevel3 extends Model
{
    
    protected $table = 'api_model_level3';
    protected $guarded = ['id'];
    protected $casts = [
        'id' => 'int',
        'data' => 'array',
        'created_at' => 'date'
   ];
   public function details()
   {
       return $this->hasMany('App\Models\Api\ApiModelsLevel4', 'parent_id', 'id');
   }
    
}
