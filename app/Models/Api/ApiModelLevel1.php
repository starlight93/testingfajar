<?php

namespace App\Models\Api;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class ApiModelLevel1 extends Model
{
    
    protected $table = 'api_model_level1';
    protected $guarded = ['id'];
    protected $casts = [
        'id' => 'int',
        'data' => 'array'
   ];
   protected $hidden = ["laravel_through_key"];
   public function meta_details()
   {
       return $this->hasMany('App\Models\Api\ApiModelLevel2', 'parent_id', 'id');
   }
    
   
   public function getCreatedAtAttribute($date)
   {
       return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y');
   }

   public function getUpdatedAtAttribute($date)
   {
       return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y');
   }
}
