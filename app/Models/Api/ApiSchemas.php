<?php

namespace App\Models\Api;

use Illuminate\Database\Eloquent\Model;

class ApiSchemas extends Model
{
    
    protected $table = 'api_schemas';
    protected $guarded = ['id'];
    protected $casts = [
        'id' => 'int',
        'schema' => 'array'
   ];
    
}
