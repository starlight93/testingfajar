<?php

namespace App\Models\Api;

use Illuminate\Database\Eloquent\Model;

class ApiModelLevel4 extends Model
{
    
    protected $table = 'api_model_level4';
    protected $guarded = ['id'];
    protected $casts = [
        'id' => 'int',
        'data' => 'array',
        'created_at' => 'date'
   ];
    
}
