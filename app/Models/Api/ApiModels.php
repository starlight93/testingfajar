<?php

namespace App\Models\Api;

use Illuminate\Database\Eloquent\Model;

class ApiModels extends Model
{
    
    protected $table = 'api_models';
    protected $guarded = ['id'];
    protected $casts = [
        'id' => 'int',
        'meta' => 'array'
   ];
    
}
