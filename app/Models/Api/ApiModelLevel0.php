<?php

namespace App\Models\Api;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class ApiModelLevel0 extends Model
{
    
    protected $table = 'api_model_level0';
    protected $guarded = ['id'];
    protected $casts = [
        'id' => 'int',
        'data' => 'array'
   ];
   protected $hidden = ["laravel_through_key"];

   public function meta_details()
   {
       return $this->hasMany('App\Models\Api\ApiModelLevel1', 'parent_id', 'id');
   }
   
   public function meta_sub_details()
   {
       return $this->hasManyThrough('App\Models\Api\ApiModelLevel2','App\Models\Api\ApiModelLevel1', 'parent_id', 'parent_id','id','id');
   }
   public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y');
    }
    // public function getIdAttribute($id)
    // {
    //     return md5($id);
    // }
    public function getDataAttribute($data)
    {
        return json_decode($data,true);
    }
}
