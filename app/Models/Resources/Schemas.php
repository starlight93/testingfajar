<?php

namespace App\Models\Resources;

use Illuminate\Database\Eloquent\Model;

class Schemas extends Model
{
    
    protected $table = 'res_schemas';
    protected $guarded = ['id'];
    protected $casts = [
        'id' => 'int',
        'schema' => 'array'
   ];
    
}
