<?php

namespace App\Models\Resources;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Models extends Model
{
    
    protected $table = 'res_models';
    protected $guarded = ['id'];
    protected $casts = [
        'id' => 'int',
        'meta' => 'array'
   ];

   public function level0()
   {
       return $this->hasMany('App\Models\Resources\ModelLevel0', 'model_id', "id");
   }
   public function level1()
   {
       return $this->hasMany('App\Models\Resources\ModelLevel1', 'model_id', "id");
   }
   public function level2()
   {
       return $this->hasMany('App\Models\Resources\ModelLevel2', 'model_id', "id");
   }
   public function schemas()
   {
       return $this->hasMany('App\Models\Resources\Schemas', 'model_id', "id");
   }
   public function getCreatedAtAttribute($date)
   {
       return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y h:i');
   }

   public function getUpdatedAtAttribute($date)
   {
       return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y h:i');
   }
}
