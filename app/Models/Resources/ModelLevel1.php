<?php

namespace App\Models\Resources;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class ModelLevel1 extends Model
{
    
    protected $table = 'res_model_1';
    protected $guarded = ['id'];
    protected $casts = [
        'id' => 'int',
        'data' => 'array'
   ];
   protected $hidden = ["laravel_through_key"];
   public function subDetails()
   {
       return $this->hasMany('App\Models\Resources\ModelLevel2', 'parent_id', 'id');
   }
    
   public function getCreatedAtAttribute($date)
   {
       return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y');
   }

   public function getUpdatedAtAttribute($date)
   {
       return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y');
   }
}
