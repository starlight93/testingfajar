<?php

namespace App\Models\Resources;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class ModelLevel0 extends Model
{
    
    protected $table = 'res_model_0';
    protected $guarded = ['id'];
    protected $casts = [
        'id' => 'int',
        'data' => 'array'
   ];
   protected $hidden = ["laravel_through_key"];

   public function details()
   {
       return $this->hasMany('App\Models\Resources\ModelLevel1', 'parent_id', 'id');
   }
   
   public function subDetails()
   {
       return $this->hasManyThrough('App\Models\Resources\ModelLevel2','App\Models\Resources\ModelLevel1', 'parent_id', 'parent_id','id','id');
   }
   public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y');
    }
    public function getDataAttribute($data)
    {
        return json_decode($data,true);
    }
}
