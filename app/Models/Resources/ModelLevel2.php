<?php

namespace App\Models\Resources;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class ModelLevel2 extends Model
{
    
    protected $table = 'res_model_2';
    protected $guarded = ['id'];
    protected $casts = [
        'id' => 'int',
        'data' => 'array'
   ];
   protected $hidden = ["laravel_through_key"];
   
   public function getCreatedAtAttribute($date)
   {
       return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y');
   }

   public function getUpdatedAtAttribute($date)
   {
       return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y');
   }
}
