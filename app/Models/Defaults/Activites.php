<?php

namespace App\Models\Defaults;

use Illuminate\Database\Eloquent\Model;

class Activities extends Model
{
    protected $table = 'default_activities';
    protected $guarded = ['id'];
}
