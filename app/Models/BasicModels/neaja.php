<?php

namespace App\Models\BasicModels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class neaja extends Model
{    
    protected $table    = 'neaja';
    protected $guarded  = ['id'];
    protected $casts    = [
        'id' => 'int',
        'created_at' => 'datetime:d-m-Y',
        'updated_at' => 'datetime:d-m-Y'
    ];
    public $lastUpdate  = "17/10/2019 16:13:25";
    public $columns     = ["id","aloha","aloha2","detail_id","created_at","updated_at"];
    public $joins       = ["item_detail.id=neaja.detail_id"];
    public $required    = ["detail_id"];
    
    
    public function item_detail()
    {
        return $this->belongsTo('App\Models\BasicModels\item_detail', 'detail_id', 'id');
    }
    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y H:i');
    }
    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y H:i');
    }
}
