<?php

namespace App\Models\BasicModels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class approval extends Model
{    
    protected $table    = 'approval';
    protected $guarded  = ['id'];
    protected $casts    = [
        'id' => 'int',
        'created_at' => 'datetime:d-m-Y',
        'updated_at' => 'datetime:d-m-Y'
    ];
    public $lastUpdate  = "17/10/2019 16:13:25";
    public $columns     = ["id","table_name","alias","participants","status","created_at","updated_at"];
    public $joins       = [];
    public $required    = [];
    
    public function item_detail()
    {
        return $this->hasMany('App\Models\BasicModels\item_detail', 'approval_id', 'id');
    }
    
    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y H:i');
    }
    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y H:i');
    }
}
