<?php

namespace App\Models\BasicModels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class approval_detail extends Model
{    
    protected $table    = 'approval_detail';
    protected $guarded  = ['id'];
    protected $casts    = [
        'id' => 'int',
        'created_at' => 'datetime:d-m-Y',
        'updated_at' => 'datetime:d-m-Y'
    ];
    public $lastUpdate  = "17/10/2019 16:13:25";
    public $columns     = ["id","approval_id","level","user_id","authority","status_name","created_at","updated_at"];
    public $joins       = [];
    public $required    = [];
    
    
    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y H:i');
    }
    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y H:i');
    }
}
