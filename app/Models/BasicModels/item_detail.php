<?php

namespace App\Models\BasicModels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class item_detail extends Model
{    
    protected $table    = 'item_detail';
    protected $guarded  = ['id'];
    protected $casts    = [
        'id' => 'int',
        'created_at' => 'datetime:d-m-Y',
        'updated_at' => 'datetime:d-m-Y'
    ];
    public $lastUpdate  = "17/10/2019 16:13:25";
    public $columns     = ["id","master_id","approval_id","name","label","qty","created_at","updated_at","deleted_at"];
    public $joins       = ["item_master.id=item_detail.master_id","approval.id=item_detail.approval_id"];
    public $required    = [];
    
    public function neaja()
    {
        return $this->hasMany('App\Models\BasicModels\neaja', 'detail_id', 'id');
    }
    
    public function item_master()
    {
        return $this->belongsTo('App\Models\BasicModels\item_master', 'master_id', 'id');
    }
    public function approval()
    {
        return $this->belongsTo('App\Models\BasicModels\approval', 'approval_id', 'id');
    }
    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y H:i');
    }
    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y H:i');
    }
}
