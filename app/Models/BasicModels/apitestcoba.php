<?php

namespace App\Models\BasicModels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class apitestcoba extends Model
{    
    protected $table    = 'apitestcoba';
    protected $guarded  = ['id'];
    protected $casts    = [
        'id' => 'int',
        'created_at' => 'datetime:d-m-Y',
        'updated_at' => 'datetime:d-m-Y'
    ];
    public $lastUpdate  = "17/10/2019 16:13:25";
    public $columns     = ["id","aloha","aloha2","created_at","updated_at"];
    public $joins       = [];
    public $required    = [];
    
    
    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y H:i');
    }
    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y H:i');
    }
}
