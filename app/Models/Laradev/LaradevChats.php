<?php

namespace App\Models\Laradev;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class LaradevChats extends Model
{
    protected $table = "laradev_chats";
    protected $guarded = ["id"];
    protected $hidden = ["laravel_through_key"];
    protected $casts = [
        'created_at' => 'datetime:d-m-Y',
        'updated_at' => 'datetime:d-m-Y'
    ];

}