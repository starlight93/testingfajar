<?php

namespace App\Models\Laradev;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class LaradevTables extends Model
{
    protected $table = "laradev_tables";
    protected $guarded = ["id"];
    protected $hidden = ["laravel_through_key"];
    protected $casts = [
        'created_at' => 'datetime:d-m-Y',
        'updated_at' => 'datetime:d-m-Y',
        'columns'    => 'array',
        'actual_columns'    => 'array',
        'actual_FK'    => 'array',
        'actual_indexes'    => 'array',
        'is_updated' => 'boolean'
    ];

    public function getActualColumnsAttribute($data)
    {
        return json_decode($data,true);
    }
}