<?php

namespace App\Models\Laradev;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class LaradevDBConnections extends Model
{
    protected $table = "laradev_dbconnections";
    protected $guarded = ["id"];
    protected $hidden = ["laravel_through_key"];
    protected $casts = [
        'created_at' => 'datetime:d-m-Y',
        'updated_at' => 'datetime:d-m-Y',
        'connection' => 'array'
    ];
    public function tables()
    {
        return $this->hasMany('App\Models\Laradev\LaradevTables', 'laradev_dbconnections_id', 'id');
    }

}