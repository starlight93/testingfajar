<?php

namespace App\Models\Laradev;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class LaradevForm extends Model
{
    protected $table = "laradev_forms";
    protected $guarded = ["id"];
    protected $hidden = ["laravel_through_key"];
    protected $casts = [
        'created_at' => 'datetime:d-m-Y',
        'updated_at' => 'datetime:d-m-Y',
        'api'        => 'array',
        'forms'      => 'array'
    ];
   
   public function comments()
   {
       return $this->hasMany('App\Models\Laradev\LaradevFormComments', 'laradev_forms_id', 'id');
   }
   public function tables()
   {
       return $this->hasMany('App\Models\Laradev\LaradevTables', 'laradev_forms_id', 'id');
   }
}