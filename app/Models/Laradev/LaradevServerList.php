<?php

namespace App\Models\Laradev;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class LaradevServerList extends Model
{
    protected $table = "laradev_serverlist";
    protected $guarded = ["id"];
    protected $hidden = ["laravel_through_key"];
    protected $casts = [
        'created_at' => 'datetime:d-m-Y',
        'updated_at' => 'datetime:d-m-Y',
        'env'   => 'array'
    ];

    public function getEnvAttribute($data)
    {
        return json_decode($data,true);
    }
}