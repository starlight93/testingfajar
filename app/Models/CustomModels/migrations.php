<?php

namespace App\Models\CustomModels;

class migrations extends \App\Models\BasicModels\migrations
{
    public $lastUpdate  = "17/10/2019 16:13:25";
    public function search($request)
    {
        $columns = $request->columns;
        $string  = strtolower($request->string);
        $data = $this->select()->where(
            function ($query)use($columns,$string) {
            foreach($columns as $column){
                if(!in_array(strtolower($column),$this->columns)){
                    continue;
                }
                $query->orWhere("LOWER($column)","like","'%$string%'");
            }
        })->orderBy('id','desc');
        return $data->paginate(50);                
    }

//======================================================================CREATE
    public function beforeCreate($model, $request){
        $newModel = $model;      //CUSTOM MODIFIED MODEL
        $newRequest  = $request; //ADD MERGE / EXCEPT ARRAY INJECTIONS
        return [
            "model"     => $newModel,
            "request"   => $newRequest,
        ];
    }

    public function afterCreate($createdModel, $request){        
        $response = null;
        return $response;
    }

//======================================================================UPDATE

    public function beforeUpdate($model, $request){
        $newModel = $model;      //CUSTOM MODIFIED MODEL
        $newRequest  = $request; //ADD MERGE / EXCEPT ARRAY INJECTIONS
        return [
            "model"     => $newModel,
            "request"   => $newRequest,
        ];
    }

    public function afterUpdate($updatedModel, $request){
        $response = null;
        return $response;
    }

//======================================================================DELETE

    public function beforeDelete($model, $request){
        $newModel = $model;      //CUSTOM MODIFIED MODEL
        $newRequest  = $request; //ADD MERGE / EXCEPT ARRAY INJECTIONS
        return [
            "model"     => $newModel,
            "request"   => $newRequest,
        ];
    }

    public function afterDelete($deletedModel, $request){
        $response = null;
        return $response; 
    }
//============================================================================
}
