

    <h4 style="text-align:right;font-weight:bold;margin-bottom:1px">
        PT. Sukses Sejahtera
    </h4>
    <h4 style="text-align:center;margin-top:1px;margin-bottom:1px">
        REPORT GENERATOR SAMPLE<br>SUMMARY
    </h4>
    <p style="text-align:center;">
        PERIODE
    </p>

<div id="parent" style="display:flex;margin-bottom:30px">
<table>
    <tr><td>
        <br><br>
        <table>
            <tr>
                <td><b style="font-size: 10px">KODE CUSTOMER</b></td><td> : 00000001</td>
            </tr>
            <tr>
                <td><b style="font-size: 10px">NAMA CUSTOMER</b></td><td> : FAJAR</td>
            </tr>
        </table>
</td>
<td style="width:35%"> </td>
<td>
        <table>
            <tr>
                <td style="border:none"></td>
                <td><b style="font-size: 10px"> TGL</b></td><td><b style="font-size: 10px"> OLEH</b></td>
            </tr>
            <tr>
                <td style="border: 1px solid #000000;"><b style="font-size: 10px;"> DIBUAT</b></td>
                <td style="border: 1px solid #000000;"> 12-12-2019</td><td style="border: 1px solid #000000;font-size:10px"> 00000001</td>
            </tr>
            <tr>
                <td style="border: 1px solid #000000;"><b style="font-size: 10px;"> DISETUJUI</b></td>
                <td style="border: 1px solid #000000;"> 12-12-2019</td><td style="border: 1px solid #000000;font-size:10px"> FAJAR FIRMANSYAH</td>
            </tr>
            <tr  style="border:none">
                <td style="border:none"><b style="font-size: 10px;"> STATUS :</b></td>
                <td colspan="2" style="border:none;text-align:left"> PROCESS</td>
            </tr>
        </table>
</td>
</tr>
</table>
</div>


<div style="padding:auto">
<table style="font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif;border-collapse: collapse; width: 100%;margin:auto;">
    <thead style="background-color: #4CAF50">
    <tr>
        <th style="border: 1px solid #ddd;
            padding: 8px;
            text-align: center;padding-top: 12px;
            padding-bottom: 12px;
            text-align: center;
            background-color: #454443;
            color: white; width:15%">No</th>
        <th style="border: 1px solid #ddd;
            padding: 8px;
            text-align: center;padding-top: 12px;
            padding-bottom: 12px;
            text-align: center;
            background-color: #454443;
            color: white; width:17%">ID</th>
        <th style="border: 1px solid #ddd;
            padding: 8px;
            text-align: center;padding-top: 12px;
            padding-bottom: 12px;
            text-align: center;
            background-color: #454443;
            color: white; width:17%">Code</th>
        <th style="border: 1px solid #ddd;
            padding: 8px;
            text-align: center;padding-top: 12px;
            padding-bottom: 12px;
            text-align: center;
            background-color: #454443;
            color: white; width:17%">Name</th>
        <th style="border: 1px solid #ddd;
            padding: 8px;
            text-align: center;padding-top: 12px;
            padding-bottom: 12px;
            text-align: center;
            background-color: #454443;
            color: white; width:17%">Harga</th>
        <th style="border: 1px solid #ddd;
            padding: 8px;
            text-align: center;padding-top: 12px;
            padding-bottom: 12px;
            text-align: center;
            background-color: #454443;
            color: white; width:17%">Status</th>
    </tr>
</thead>
    <tbody>
        @foreach($data as $row)
            <tr>
                <td style="border: 1px solid #ddd;padding: 8px;text-align: center">{{$loop->iteration}}</td>
                <td style="border: 1px solid #ddd;padding: 8px;text-align: center">{{$row->id}}</td>
                <td style="border: 1px solid #ddd;padding: 8px;text-align: center">{{$row->code}}</td>
                <td style="border: 1px solid #ddd;padding: 8px;text-align: center">{{$row->name}}</td>
                <td style="border: 1px solid #ddd;padding: 8px;text-align: center">{{$row->harga}}</td>
                <td style="border: 1px solid #ddd;padding: 8px;text-align: center">{{$row->status}}</td>
            </tr>
        @endforeach
    </tbody>
</table>
</div>
