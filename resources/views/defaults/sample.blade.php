<html>
    <head>
        <title>Laradev Versi 2</title>
        <link rel="stylesheet" href="defaults/request.css">
        <link rel="stylesheet" href="defaults/codemirror.css">
        <link rel="stylesheet" href="defaults/theme/monokai.css">
        <link rel="stylesheet" href="defaults/addon/lint/lint.css">
    </head>
    <body>
<input type=hidden id="urlCurrent" value="{{url()}}"">
<div id="codemirror">
    <p class="judul" id="judul">REQUEST</p>
    <p class="endpoint" id="url"></p>
    <textarea id="code">
    </textarea>
    </p><a href="javascript:void(0)" class="button" id="run">Run on Console!</a></p>
</div>

<div>
    <p class="title">REGISTER</p>
    <textarea class="samplecode" readonly>
{
    "url"       : "/register",
    "method"    : "POST",
    "headers"   :{
    },
    "body"  : {
        "name"    : "Nama Anda",
        "email"   : "EmailAnda@emailAnda.com",
        "password": "passwordAnda",
        "password_confirmation" : "passwordAnda"
    }
}
    </textarea>
    </p><button href="javascript:void(0)">Copy to Editor</button></p>
</div>


<div>
    <p class="title">LOGIN</p>
    <textarea class="samplecode" readonly>
{
    "url"       : "/login",
    "method"    : "POST",
    "headers"   :{
    },
    "body"  : {
        "email"   :"email@email.com",
        "password":"pass"
    }
}
    </textarea>
    </p><button href="javascript:void(0)">Copy to Editor</button></p>
</div>

<div>
    <p class="title">GET ME</p>
    <textarea class="samplecode" readonly>
{
    "url"       : "/me",
    "method"    : "GET",
    "headers"   :{
        "authorization"  : "token setelah login"
    },
    "body"  : {
    }
}
    </textarea>
    </p><button href="javascript:void(0)">Copy to Editor</button></p>
</div>

<div>
    <p class="title">CREATE</p>
    <textarea class="samplecode" readonly>
{
    "url"       : "/api-test",
    "method"    : "POST",
    "headers"   :{
        "authorization"  : "token setelah login"
    },
    "body"  : {
        "meta_process": "GOES Marketing Master Item",
        "meta_model": "header1",
        "kolom1": "datax",
        "kolom2": "datay",
        "meta_details": {
            "childLevel1_1": [
                {
                    "kolom1": "dataku",
                    "kolom2": "datamu",
                    "meta_details": {
                        "childLevel2_1": [
                            {
                                "kolom1": "datadia",
                                "kolom2": "datanya"
                            }
                        ]
                    }
                },
                {
                    "ini_detail": 2
                },
                {
                    "ini_detail": 3
                }
            ],
            "childLevel2_2": [
                {
                    "kolom1": "dataku",
                    "kolom2": "datamu",
                    "meta_details": {
                        "childLevel2_1": [
                            {
                                "kolom1": "datadia",
                                "kolom2": "datamereka"
                            }
                        ]
                    }
                },
                {
                    "ini_detail": 2
                },
                {
                    "ini_detail": 3
                }
            ]
        }
    }
}
    </textarea>
    </p><button href="javascript:void(0)">Copy to Editor</button></p>
</div>

<div>
    <p class="title">SELECT:LIST</p>
    <textarea class="samplecode" readonly>
{
    "url"       : "/api-test",
    "method"    : "GET",
    "headers"   :{
        "authorization"  : "token setelah login"
    },
    "body"  : {
        "meta_process": "GOES Marketing Master Item",
        "meta_model"  : "header1",
        "meta_search" : "data",
        "meta_cascade": false
    }
}
    </textarea>
    </p><button href="javascript:void(0)">Copy to Editor</button></p>
</div>

<div>
    <p class="title">DELETE</p>
    <textarea class="samplecode" readonly>
{
    "url"       : "/api-test",
    "method"    : "DELETE",
    "headers"   :{
        "authorization"  : "token setelah login"
    },
    "body"  : {
        "meta_process"  : "GOES Marketing Master Item",
        "meta_model"    : "header1",
        "meta_where"    : "id=7",
        "meta_cascade"  : false
    }
}
    </textarea>
    </p><button href="javascript:void(0)">Copy to Editor</button></p>
</div>

<div>
    <p class="title">LIST MODELS</p>
    <textarea class="samplecode" readonly>
{
    "url"       : "/model-list",
    "method"    : "GET",
    "headers"   :{
        "authorization"  : "token setelah login"
    },
    "body"  : {
        "where"     : "level=0",
        "paginate"  : 2,
        "page"      : 1
    }
}
    </textarea>
    </p><button href="javascript:void(0)">Copy to Editor</button></p>
</div>

        <script src="defaults/axios.min.js"></script>
            <script src="defaults/codemirror.js"></script>
            <script src="defaults/addon/mode/loadmode.js"></script>
            <script src="defaults/addon/mode/javascript.js"></script>
            <script src="defaults/addon/search/searchcursor.js"></script>
            <script src="defaults/addon/search/search.js"></script>
            <script src="defaults/addon/dialog/dialog.js"></script>
            <script src="defaults/addon/edit/matchbrackets.js"></script>
            <script src="defaults/addon/edit/closebrackets.js"></script>
            <script src="defaults/addon/comment/comment.js"></script>
            <script src="defaults/addon/wrap/hardwrap.js"></script>
            <script src="defaults/addon/fold/foldcode.js"></script>
            <script src="defaults/addon/fold/brace-fold.js"></script>
            <script src="defaults/addon/keymaps/sublime.js"></script>
            <script src="defaults/addon/edit/matchbrackets.js"></script>
            <script src="defaults/addon/comment/continuecomment.js"></script>
            <script src="defaults/addon/comment/comment.js"></script>
            <script src="defaults/addon/lint/jshint.js"></script>
            <script src="defaults/addon/lint/lint.js"></script>
            <script src="defaults/addon/lint/javascript-lint.js"></script>
            <script src="defaults/addon/lint/css-lint.js"></script>
            <script src="defaults/request.js"></script>
        <script src="defaults/autosize.js"></script>
    </body>
</html>