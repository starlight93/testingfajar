<html>
    <head>
        <title>Laradev Versi 2</title>
        <link rel="stylesheet" href="defaults/request.css">
        <link rel="stylesheet" href="defaults/codemirror.css">
        <link rel="stylesheet" href="defaults/theme/monokai.css">
        <link rel="stylesheet" href="defaults/addon/lint/lint.css">
    </head>
    <body>
<input type=hidden id="urlCurrent" value="{{url()}}"">
<div id="codemirror">
    <p class="judul" id="judul">REQUEST</p>
    <p class="endpoint" id="url"></p>
    <textarea id="code">
    </textarea>
    </p><a href="javascript:void(0)" class="button" id="run">Run on Console!</a></p>
</div>

<div>
    <p class="title">REGISTER</p>
    <textarea class="samplecode" readonly>
{
    "url"       : "/register",
    "method"    : "POST",
    "headers"   :{
    },
    "body"  : {
        "name"    : "Nama Anda",
        "email"   : "EmailAnda@emailAnda.com",
        "password": "passwordAnda",
        "password_confirmation" : "passwordAnda"
    }
}
    </textarea>
    </p><button href="javascript:void(0)">Copy to Editor</button></p>
</div>


<div>
    <p class="title">LOGIN</p>
    <textarea class="samplecode" readonly>
{
    "url"       : "/login",
    "method"    : "POST",
    "headers"   :{
    },
    "body"  : {
        "email"   :"email@email.com",
        "password":"pass"
    }
}
    </textarea>
    </p><button href="javascript:void(0)">Copy to Editor</button></p>
</div>

<div>
    <p class="title">GET ME</p>
    <textarea class="samplecode" readonly>
{
    "url"       : "/me",
    "method"    : "GET",
    "headers"   :{
        "authorization"  : "token setelah login"
    },
    "body"  : {
    }
}
    </textarea>
    </p><button href="javascript:void(0)">Copy to Editor</button></p>
</div>

<div>
    <p class="title">CREATE</p>
    <textarea class="samplecode" readonly>
{
    "url"       : "/resources/**projectAnda/**modelAnda",
    "method"    : "POST",
    "headers"   :{
        "authorization"  : "token setelah login"
    },
    "body": {
      "kolomheader1": "data",
      "kolomheader2": "header1",
      "detail_1": [
            {
              "kolomdetail1":"halo",
              "kolomdetail2":"halo",
              "detail_12":[
                {
                  "kolomsubdetail1":1,
                  "kolomsubdetail2":2
                }  
              ]
            }        
          ],
      "detail_2":[
            {
              "kolomdetail2":"halo"
            }
          ]
    }
}
    </textarea>
    </p><button href="javascript:void(0)">Copy to Editor</button></p>
</div>

<div>
    <p class="title">UPDATE</p>
    <textarea class="samplecode" readonly>
{
    "url"       : "/resources/**projectAnda/**modelAnda/**id",
    "method"    : "PUT",
    "headers"   :{
        "authorization"  : "token setelah login"
    },
    "body": {
      "kolomheader1": "data",
      "kolomheader2": "header1",
      "detail_1": [
            {
              "kolomdetail1":"halo",
              "kolomdetail2":"halo",
              "detail_12":[
                {
                  "kolomsubdetail1":1,
                  "kolomsubdetail2":2
                }  
              ]
            }        
          ],
      "detail_2":[
            {
              "kolomdetail2":"halo"
            }
          ]
    }
}
    </textarea>
    </p><button href="javascript:void(0)">Copy to Editor</button></p>
</div>

<div>
    <p class="title">SELECT:LIST</p>
    <textarea class="samplecode" readonly>
{
    "url"       : "/resources/**projectAnda/**modelAnda?orderby=id&ordertype=desc&paginate=100&where=id>0",
    "method"    : "GET",
    "headers"   :{
        "authorization"  : "token setelah login"
    },
    "body"  : {}
}
    </textarea>
    </p><button href="javascript:void(0)">Copy to Editor</button></p>
</div>

<div>
    <p class="title">SELECT:MODEL per ID</p>
    <textarea class="samplecode" readonly>
{
    "url"       : "/resources/**projectAnda/**modelAnda/**id",
    "method"    : "GET",
    "headers"   :{
        "authorization"  : "token setelah login"
    },
    "body"  : {}
}
    </textarea>
    </p><button href="javascript:void(0)">Copy to Editor</button></p>
</div>

<div>
    <p class="title">DELETE</p>
    <textarea class="samplecode" readonly>
{
    "url"       : "/resources/**projectAnda/**modelAnda/**id",
    "method"    : "DELETE",
    "headers"   :{
        "authorization"  : "token setelah login"
    },
    "body"  : {}
}
    </textarea>
    </p><button href="javascript:void(0)">Copy to Editor</button></p>
</div>

<div>
    <p class="title">LIST MODELS</p>
    <textarea class="samplecode" readonly>
{
    "url"       : "/resources-models",
    "method"    : "GET",
    "headers"   :{
        "authorization"  : "token setelah login"
    },
    "body"  : {}
}
    </textarea>
    </p><button href="javascript:void(0)">Copy to Editor</button></p>
</div>

        <script src="defaults/axios.min.js"></script>
            <script src="defaults/codemirror.js"></script>
            <script src="defaults/addon/mode/loadmode.js"></script>
            <script src="defaults/addon/mode/javascript.js"></script>
            <script src="defaults/addon/search/searchcursor.js"></script>
            <script src="defaults/addon/search/search.js"></script>
            <script src="defaults/addon/dialog/dialog.js"></script>
            <script src="defaults/addon/edit/matchbrackets.js"></script>
            <script src="defaults/addon/edit/closebrackets.js"></script>
            <script src="defaults/addon/comment/comment.js"></script>
            <script src="defaults/addon/wrap/hardwrap.js"></script>
            <script src="defaults/addon/fold/foldcode.js"></script>
            <script src="defaults/addon/fold/brace-fold.js"></script>
            <script src="defaults/addon/keymaps/sublime.js"></script>
            <script src="defaults/addon/edit/matchbrackets.js"></script>
            <script src="defaults/addon/comment/continuecomment.js"></script>
            <script src="defaults/addon/comment/comment.js"></script>
            <script src="defaults/addon/lint/jshint.js"></script>
            <script src="defaults/addon/lint/lint.js"></script>
            <script src="defaults/addon/lint/javascript-lint.js"></script>
            <script src="defaults/addon/lint/css-lint.js"></script>
            <script src="defaults/request.js"></script>
        <script src="defaults/autosize.js"></script>
    </body>
</html>